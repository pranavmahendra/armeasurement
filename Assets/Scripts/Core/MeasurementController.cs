using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(ARRaycastManager))]
public class MeasurementController : MonoBehaviour {

    private const int _DEFAULT_LABEL_INDEX = -1;
    private const int DEFAULT_LINE_INDEX = 0;
    private const int DEFAULT_CURRENT_VERT_COUNT = 1;

    [SerializeField] private Camera arCamera;
    [SerializeField] private GameObject selectedPointPrefab;
    [SerializeField] private float measurementFactor = 39.37f;
    [SerializeField] private PlacementTracker tracker;

    private DistanceLabelHandler _trackerDistanceLabel;
    private DistanceLabelHandler _currentDistanceLabel;
    private List<GameObject> _pointCollection = new List<GameObject>();
    private LineRenderer _lineRenderer;
    private GameObject _currentPoint;
    private float _distanceCalculator;
    private float _setLength;
    private float _setWidth;
    private int _forLabelIndex;
    private int _lineIndex;
    private int _currentVertCounts;

    public Vector3 GetPoint(int index) => _pointCollection[index].transform.position;
    public int PointCount => _pointCollection.Count;

    public float SetLength => _setLength;
    public float SetWidth => _setWidth;

    private void Awake()
    {
        this._lineRenderer = GetComponent<LineRenderer>();
        this._trackerDistanceLabel = tracker.PlacementDistanceLabel;
        this._lineRenderer.enabled = false;

        SetInitialValues();
    }

    private void OnEnable()
    {
        NullCheck();
    }


    void Update()
    {
        SetMeasurementPositions();
    }

    private void LateUpdate()
    {

        if ( _lineRenderer.positionCount > 0 ) {
            LabelUpdater(_lineRenderer.GetPosition(_forLabelIndex));
        }
    }

    private void SetMeasurementPositions()
    {
        if ( tracker.Indicator != null && Input.touchCount > 0 ) {
            Touch _touch = Input.GetTouch(0);

            if ( _touch.phase == TouchPhase.Began && !EventSystem.current.IsPointerOverGameObject(_touch.fingerId) ) {

                _lineRenderer.enabled = true;
                // Add a point add visual indicator position.
                _currentPoint = InstantiatePoint();

                // Set the count of array of Line renderer.
                _lineRenderer.positionCount = _currentVertCounts;

                // Set the position of the point.
                _lineRenderer.SetPosition(_lineIndex, _currentPoint.transform.position);

                CurrentPointLabelHandler();

                _forLabelIndex++;
                _currentVertCounts++;
                _lineIndex++;

            }

            // CHeck here.
            if ( _touch.phase == TouchPhase.Ended ) {

                if ( _lineIndex == 2 ) {
                    this._setLength = _distanceCalculator;
                }
                else if ( _lineIndex == 3 ) {
                    this._setWidth = _distanceCalculator;
                }

            }
        }
    }


    private GameObject InstantiatePoint()
    {
        GameObject point = Instantiate(selectedPointPrefab, tracker.transform.position, Quaternion.identity);
        _pointCollection.Add(point);

        point.name = "Point " + _currentVertCounts;
        DistanceLabelHandler _handler = point.GetComponentInChildren<DistanceLabelHandler>();

        _currentDistanceLabel = _handler;

        _handler.InitializeCamera(arCamera);
        return point;
    }

    private void CurrentPointLabelHandler()
    {
        if ( _lineRenderer.positionCount <= 1 ) {
            _currentDistanceLabel.TextComponent.text = _trackerDistanceLabel.TextComponent.text = $"Distance: {0} in";
        }
        else {
            _distanceCalculator = Vector3.Distance(_lineRenderer.GetPosition(_forLabelIndex), _lineRenderer.GetPosition(_lineIndex));
            _currentDistanceLabel.TextComponent.text = $"Distance: {(_distanceCalculator * measurementFactor).ToString("00")} in";
        }
    }

    private void LabelUpdater(Vector3 initialPos)
    {
        if ( _currentDistanceLabel != null ) {
            _trackerDistanceLabel.TextComponent.text = $"Distance: {(Vector3.Distance(initialPos, tracker.VisualPos) * measurementFactor).ToString("00")} in";
        }
    }

    public void ClearPoints()
    {
        for ( int i = 0; i < _pointCollection.Count; i++ ) {
            Destroy(_pointCollection[i]);
        }
        _pointCollection.Clear();
        _lineRenderer.positionCount = 0;

        SetInitialValues();
    }

    private void SetInitialValues()
    {
        _setLength = 0;
        _setWidth = 0;
        _forLabelIndex = _DEFAULT_LABEL_INDEX;
        _lineIndex = DEFAULT_LINE_INDEX;
        _currentVertCounts = DEFAULT_CURRENT_VERT_COUNT;
    }

    private void NullCheck()
    {
        if ( selectedPointPrefab == null ) {
#if UNITY_EDITOR
            Debug.Log($"Measurement Prefab is null");
#endif
        }
    }
}
