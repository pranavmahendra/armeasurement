using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIHandler : MonoBehaviour {

    [SerializeField] private Image mapPanel;
    [SerializeField] private MeasurementController measurementHandler;
    [SerializeField] private Button activateDimenPanel;
    [SerializeField] private Button deactivateDimenPanel;
    [SerializeField] private Button clearPointBtn;
    [SerializeField] private GameObject DimensionsPanelPrefab;
    [SerializeField] private TextMeshProUGUI dimensionsLabel;

    private GameObject _currentDevelopedImage;
    private float _measurementFactor = 39.37f;
    private RectTransform _mapPanelRect;

    private void Awake()
    {
        _mapPanelRect = mapPanel.GetComponent<RectTransform>();
        Debug.Log($"Dimensions for {mapPanel.gameObject.name} is Width {_mapPanelRect.rect.width} and Height {_mapPanelRect.rect.height}");
    }

    private void Start()
    {
        clearPointBtn.onClick.AddListener(measurementHandler.ClearPoints);
        activateDimenPanel.onClick.AddListener(ActivateDimensionPanel);
        deactivateDimenPanel.onClick.AddListener(DeactivateDimensionPanel);
    }

    private void ActivateDimensionPanel()
    {
        mapPanel.gameObject.SetActive(true);

        GameObject newItem = Instantiate(DimensionsPanelPrefab) as GameObject;
        _currentDevelopedImage = newItem;
        newItem.name = "DimensionsImage";

        RectTransform rect = newItem.GetComponent<RectTransform>();
        rect.SetParent(mapPanel.transform, false);

        // Calculate the width and height of each child item
        float length = measurementHandler.SetLength * 300f;
        float width = measurementHandler.SetWidth * 300f;

        dimensionsLabel.text = $"Dimensions: {(measurementHandler.SetLength * _measurementFactor).ToString("00")} in x {(measurementHandler.SetWidth * _measurementFactor).ToString("00")} in";
        dimensionsLabel.fontSize = 20f;

        rect.sizeDelta = new Vector2(length, width);
    }

    private void DeactivateDimensionPanel()
    {
        mapPanel.gameObject.SetActive(false);
        Destroy(_currentDevelopedImage);

    }
}