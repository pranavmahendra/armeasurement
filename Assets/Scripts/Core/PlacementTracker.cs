using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using TMPro;

public class PlacementTracker : MonoBehaviour {

    [SerializeField] private ARRaycastManager rayManager;
    [SerializeField] private Camera arCamera;
    [SerializeField] private GameObject visual;
    [SerializeField] private DistanceLabelHandler placementDistanceLabel;

    public DistanceLabelHandler PlacementDistanceLabel => placementDistanceLabel;
    public GameObject Indicator => visual;
    public Vector3 VisualPos => this.transform.position;
    private Vector3 _offset;

    private void Awake()
    {
        arCamera = Camera.main;
        _offset = new Vector3(0f, 0.02f, 0f);
    }

    void Start()
    {
        // hide the placement indicator visual
        visual.SetActive(false);
    }

    void Update()
    {
        PlacementIndicatorPos();
    }

    private void PlacementIndicatorPos()
    {
        // shoot a raycast from the center of the screen
        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        rayManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), hits, TrackableType.PlaneWithinPolygon);

        // if we hit an AR plane surface, update the position and rotation
        if ( hits.Count > 0 ) {
            transform.position = hits[0].pose.position;
            transform.rotation = hits[0].pose.rotation;

            // enable the visual if it's disabled
            if ( !visual.activeInHierarchy )
                visual.SetActive(true);
        }
    }

#if UNITY_EDITOR
    private void UnityRaycast()
    {
        RaycastHit hit;
        if ( Physics.Raycast(arCamera.transform.position, arCamera.transform.forward, out hit, Mathf.Infinity) ) {

            Debug.DrawRay(arCamera.transform.position, arCamera.transform.forward, Color.green, Mathf.Infinity);

            transform.position = hit.point + _offset;
            if ( !visual.activeInHierarchy )
                visual.SetActive(true);
        }
    }
#endif
}
