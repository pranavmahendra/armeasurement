using System.Collections;
using UnityEngine.XR.ARFoundation;
using UnityEngine;

public class ARAvailableStatus : MonoBehaviour
{
    private ARSession m_Session;

    private void Awake()
    {
        m_Session = GetComponent<ARSession>();
    }

    IEnumerator Start()
    {
        if ((ARSession.state == ARSessionState.None) ||
            (ARSession.state == ARSessionState.CheckingAvailability))
        {
            yield return ARSession.CheckAvailability();
        }

        if (ARSession.state == ARSessionState.Unsupported)
        {
        }
        else
        {
            m_Session.enabled = true;
        }
    }
}