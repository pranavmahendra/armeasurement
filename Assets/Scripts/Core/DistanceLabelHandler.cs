using UnityEngine;
using TMPro;

public class DistanceLabelHandler : MonoBehaviour {

    [SerializeField] private TextMeshProUGUI textComponent;

    private Canvas _labelCanvas;
    private Camera _currentCamera;

    public Canvas LabelCanvas { get { return _labelCanvas; } }
    public TextMeshProUGUI TextComponent => textComponent;

    private void Awake()
    {
        _labelCanvas = GetComponent<Canvas>();
        _currentCamera = _labelCanvas.worldCamera;
    }

    private void LateUpdate()
    {
        if ( _currentCamera != null ) {
            this.transform.LookAt(_currentCamera.transform);
            transform.Rotate(0f, 180f, 0f);
        }
    }

    public void InitializeCamera(Camera camera)
    {
        _labelCanvas.worldCamera = camera;
        _currentCamera = camera;
    }

}
